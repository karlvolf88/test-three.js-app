const { resolve } = require('path')

const loaders = require('./webpack/loaders')
const webpackResolve = require('./webpack/resolve')
const stats = require('./webpack/stats')
const plugins = require('./webpack/plugins')

module.exports = {
	context: __dirname,
	mode: 'development',
	devtool: 'source-map',
	cache: false,
	entry: [
		`webpack-hot-middleware/client?http://localhost:${process.env.HTTP_PORT}&reload=true`,
		'./source/scripts/index.jsx'
	],
	output: {
		filename: 'index.js',
		path: resolve(__dirname, 'public'),
		publicPath: '/',
	},
	stats,
	module: {
		rules: loaders
	},
	resolve: webpackResolve,
	plugins,
}
