module.exports = function () {
	return `
		<!DOCTYPE html>
		<html lang="en">
		<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
      <base href="/" />
	    <title>Test Application</title>
	    <link rel="stylesheet" href="/main.css">
		</head>
		<body>
	    <div id="app-root" class="l-app-container"></div>
	    <script src="/index.js"></script>
		</body>
		</html>
	`
}
