import * as THREE from 'three'

const {
	Scene,
	PerspectiveCamera,
	DirectionalLight,
	WebGLRenderer,
	LoadingManager,
} = THREE

import { OBJLoader2 } from 'three/examples/jsm/loaders/OBJLoader2'
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls'

export function init (container, url) {
	const containerSize = container.getBoundingClientRect()
	const scene = new Scene()

	const directionalLight = new DirectionalLight(0xffffff)
	directionalLight.position.set(0, 1, 1)
	scene.add(directionalLight)

	const fov = 75 // поле зрения
	const aspect = 2 // соотношение сторон
	const near = 0.1 // пространство перед камерой
	const far = 5 // пространство перед камерой
	const camera = new PerspectiveCamera(fov, aspect, near, far)
	camera.position.z = 5

	const renderer = new WebGLRenderer({ alpha: true })
	renderer.setSize(containerSize.width, containerSize.width / 1.5 )
	container.appendChild(renderer.domElement)

	const controls = new TrackballControls( camera, renderer.domElement )
	controls.rotateSpeed = 1.0
	controls.zoomSpeed = 1.2
	controls.panSpeed = 0.8
	controls.keys = [ 65, 83, 68 ]

	const manager = new LoadingManager()
	manager.onProgress = function ( item, loaded, total ) {
		console.log( item, loaded, total )
	}

	const loader = new OBJLoader2(manager)

	url.forEach((urlItem, index) => {
		loader.load(`${urlItem}`, function (object) {
			scene.add(object)
			object.position.x = 1
			object.position.y = 1
			object.position.z = index * 5
		})
		render()
	})

	function render () {
		controls.update()
		window.requestAnimationFrame(render)
		renderer.render(scene, camera)
	}

	render()
}
