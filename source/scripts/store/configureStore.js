import { createBrowserHistory } from 'history'
import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import ReduxThunk from 'redux-thunk'

import rootReducer from './rootReducer'

export const history = createBrowserHistory()

export default function (initialState = {}) {
	const reduxExtension = typeof window !== 'undefined' && window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']

	const composeEnhancer = (
		typeof reduxExtension !== 'undefined' &&
		typeof window !== 'undefined'
	) ? reduxExtension({ trace: true, traceLimit: 25 }) : compose

	const middleware = [
		ReduxThunk,
		routerMiddleware(history)
	]

	const enhancer = composeEnhancer(
		applyMiddleware(
			...middleware
		)
	)

	const store = createStore(
		rootReducer(history),
		{ ...initialState },
		enhancer
	)

	if (module['hot']) {
		module['hot'].accept()
		// Enable Webpack hot module replacement for reducers
		module['hot'].accept('./rootReducer.js', () => {
			store.replaceReducer(rootReducer())
		})
	}

	return store
}

