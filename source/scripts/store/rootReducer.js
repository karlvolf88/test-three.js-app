import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import init from './reducers/init'
import page from './reducers/page'
import request from './reducers/request'
import list from './reducers/list'
import select from './reducers/select'

const rootReducer = (history) => combineReducers({
	router: connectRouter(history),
	request,
	page,
	init,
	list,
	select
})

export default rootReducer
