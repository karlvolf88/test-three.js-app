const initialState = {
	status: null,
	isFetching: false
}

const request = (state = initialState, action) => {
	if (action.type === 'app@REQUEST') {
		return { ...state, ...action.payload }
	}
	return state
}

export default request
