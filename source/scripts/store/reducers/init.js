const initialState = {
	initialized: false,
	isLoading: false,
	isLoad: false,
}

const init = (state = initialState, action) => {
	if (action.type === 'app@INIT') {
		return { ...state, ...action.payload }
	}
	return state
}

export default init
