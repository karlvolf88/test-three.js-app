const initialState = {
	currentPage: window.location.pathname || '/'
}

const page = (state = initialState, action) => {
	if (action.type === 'app@PAGE') {
		return { ...state, ...action.payload }
	}
	return state
}

export default page
