const initialState = {
	list: []
}

const select = (state = initialState, action) => {
	if (action.type === 'app@SELECT') {
		return { ...state, ...action.payload }
	}
	return state
}

export default select
