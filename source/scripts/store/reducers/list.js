const initialState = {
	status: null,
	isFetching: false,
	list: []
}

const list = (state = initialState, action) => {
	if (action.type === 'app@REQUEST_LIST') {
		return { ...state, ...action.payload }
	}
	return state
}

export default list
