export function page (payload) {
	return {
		type: 'app@PAGE',
		payload
	}
}
