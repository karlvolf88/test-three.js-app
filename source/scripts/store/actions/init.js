export function init (payload) {
	return {
		type: 'app@INIT',
		payload
	}
}
