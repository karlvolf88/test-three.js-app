import axios from 'axios'

const startRequest = () => ({
	type: 'app@REQUEST',
	payload: { isFetching: true	}
})

const successRequest = (details) => ({
	type: 'app@REQUEST',
	payload: { isFetching: false,	status: 'success', details }
})

const errorRequest = (details) => ({
	type: 'app@REQUEST',
	payload: { isFetching: false,	status: 'error', details }
})

export function request (url, fileData) {
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data; charset=utf-8;'
		}
	}

	const formData = new FormData()

	formData.append(fileData['name'], fileData['value'])

	return dispatch => {
		dispatch(startRequest())
		return axios.post(url, formData, config)
			.then((response) => {
				dispatch(successRequest({ ...response.data }))
			})
			.catch(err => {
				const result = err.response && err.response.data ? err.response.data : err.response
				dispatch(errorRequest({ ...result }))
			})
	}
}
