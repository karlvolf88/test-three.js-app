import axios from 'axios'

const startRequest = () => ({
	type: 'app@REQUEST_LIST',
	payload: { isFetching: true	}
})

const successRequest = (list) => ({
	type: 'app@REQUEST_LIST',
	payload: { isFetching: false,	status: 'success', list }
})

const errorRequest = (details) => ({
	type: 'app@REQUEST_LIST',
	payload: { isFetching: false,	status: 'error', details }
})

export function requestList () {
	return dispatch => {
		dispatch(startRequest())
		return axios.post('/list', {})
			.then((response) => {
				console.log(response.data)
				dispatch(successRequest(response.data.list))
			})
			.catch(err => {
				const result = err.response && err.response.data ? err.response.data : err.response
				dispatch(errorRequest({ ...result }))
			})
	}
}
