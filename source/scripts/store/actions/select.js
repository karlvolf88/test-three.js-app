export function select (payload) {
	return {
		type: 'app@SELECT',
		payload
	}
}
