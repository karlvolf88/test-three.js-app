import React from 'react'
import PropTypes from 'prop-types'
import { render as RenderMethod } from 'react-dom'
import { Router as BrowserRouter } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'

import configureStore, { history } from './store/configureStore'

import '../styles/main.scss'

import Routing from './router/index'

const App = ({ store }) => (
	<Provider store={store}>
		<ConnectedRouter history={history} store={store} onLocationChanged={(event) => event}>
			<BrowserRouter history={history}>
				<Routing />
			</BrowserRouter>
		</ConnectedRouter>
	</Provider>
)

App.propTypes = {
	store: PropTypes.object.isRequired
}

const defaultStore = configureStore()

const ClientApp = <App store={defaultStore} />

RenderMethod(
	ClientApp,
	document.getElementById('app-root')
)
