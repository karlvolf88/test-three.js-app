export const ROUTES = [
	{
		id: 'MainPage',
		name: 'Главная',
		path: '/'
	},
	{
		id: 'UploadPage',
		name: 'Загрузка файлов',
		path: '/upload',
		mainMenu: true
	},
	{
		id: 'ListPage',
		name: 'Список файлов',
		path: '/list',
		exact: false,
		mainMenu: true
	},
	{
		id: 'ViewPage',
		name: 'Просмотр модели',
		path: '/view/:name',
	},
	{
		id: 'Page404',
		name: 'Страница не найдена',
		path: '*',
	}
]
