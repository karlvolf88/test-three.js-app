import React from 'react'

import Header from 'Container/HeaderContainer'
import Footer from 'Organism/Footer'

import { RenderRoutes } from './view'

const Routing = (props) => {
	return (
		<div className={'l-app'}>
			<Header {...props} />
			<RenderRoutes {...props} />
			<Footer />
		</div>
	)
}

export default Routing
