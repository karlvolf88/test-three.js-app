import React from 'react'
import { Switch, Route } from 'react-router-dom'

import { ROUTES as RoutesConfig } from './config'

export const RenderRoutes = () => {
	const routes = RoutesConfig.map((route, index) => {
		const Component = require(`Pages/${route.id}`).default
		return (
			<Route
				exact={route.exact || true}
				key={`route-component-${route.id}-${index}`}
				name={route.id}
				path={route.path}>
				<Component {...route} />
			</Route>
		)
	})

	return (
		<Switch>
			{routes}
		</Switch>
	)
}
