const fileUpload = require('express-fileupload')
const fs = require('fs')
const { resolve } = require('path')
const express = require('express')
const webpack = require('webpack')
// const helmet = require('helmet')
const bodyParser = require('body-parser')
// const compression =  require('compression')

const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const webpackConfig = require('../../webpack.config.client')
const compiler = webpack(webpackConfig)

const app = express()

app.use(
	webpackDevMiddleware(compiler, {
		publicPath: webpackConfig.output.publicPath,
		stats: require('../../webpack/stats')
	})
)
app.use(webpackHotMiddleware(compiler))
app.use(fileUpload({
	createParentPath: true
}))
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.post('/upload', async (request, response) => {
	const mimeType = new RegExp('.obj')

	let uploadedFile = request.files || false

	try {
		if(!uploadedFile && !uploadedFile.model) {
			response.status(400).send({
				status: false,
				message: 'Ошибка отправки файла'
			})
		} else if (!mimeType.test(uploadedFile.model.name)) {
			response.status(400).send({
				status: false,
				message: 'Неверный формат файла, используйте .obj'
			})
		} else if (fs.existsSync('./public/uploads/' + uploadedFile.model.name)) {
			response.status(400).send({
				status: false,
				message: `Файл ${uploadedFile.model.name} уже был загружен`
			})
		} else {
			uploadedFile.model.mv('./public/uploads/' + uploadedFile.model.name)
			response.send({
				status: true,
				message: `Файл ${uploadedFile.model.name} успешно загружен`,
			})
		}
	} catch (err) {
		console.log(err)
		response.status(500).send({ data: err })
	}
})

app.post('/list', async (request, response) => {
	const folder = './public/uploads'
	if (fs.existsSync(folder)) {
		fs.readdir(folder, (err, files) => {
			const filesList = []
			let counter = 0
			files.forEach(file => {
				if (file) {
					filesList.push({ name: file, id: counter, url: `/uploads/${file}` })
					counter++
				}
			})

			if (filesList.length > 0) {
				try {
					response.send({
						status: true,
						list: filesList,
						message: 'Список моделей успешно получен',
					})
				} catch (error) {
					response.status(500).send({ data: error })
				}
			} else {
				response.status(400).send({
					status: false,
					message: 'Нет загруженных моделей'
				})
			}
		})
	}
})

app.get('*', async (request, response) => {
	response.send(require('../html/index')())
})

app.listen(3000, () => {
	console.log('Example app listening on port 3000!')
})
