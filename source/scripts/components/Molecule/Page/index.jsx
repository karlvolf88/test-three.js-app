import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import './index.scss'

class Page extends PureComponent {
	static propTypes = {
		children: PropTypes.oneOfType([
			PropTypes.element,
			PropTypes.array,
			PropTypes.string
		])
	}

	render () {
		const { children } = this.props
		return (
			<div className="b-page">
				<div className="container">
					{children}
				</div>
			</div>
		)
	}
}

export default Page
