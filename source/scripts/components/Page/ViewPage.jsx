import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Page from 'Molecule/Page'
import ModelLoader from 'Organism/ModelLoader'

const ViewPage = ({ page, selectList }) => {
	useEffect(() => {
		if (!page.data && selectList.length <= 0) {
			setTimeout(() => {
				if (!page.date) {
					history.back()
				}
			}, 200)
		}
	})

	const objectList = typeof page['data'] !== 'undefined' ? [ page['data'] ] : selectList

	return (
		<Page>
			<div className="b-view">
				<ModelLoader url={objectList} />
			</div>
		</Page>
	)
}

ViewPage.propTypes = {
	dispatch: PropTypes.func,
	page: PropTypes.object,
	selectList: PropTypes.array
}

function mapStateToProps (state) {
	const { list, page, select } = state
	return {
		selectList: select.list,
		models: list,
		page
	}
}

function mapDispatchToProps (dispatch) {
	return {
		dispatch
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewPage)
