import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'

import { init } from '../../store/actions/init'
import { select } from '../../store/actions/select'
import { requestList } from '../../store/actions/list'

import Link from 'Container/LinkContainer'
import Page from 'Molecule/Page'

const ListPage = ({ dispatch, models, loadData, selectedList }) => {
	const { list } = models

	useEffect(() => {
		if (!models.isFetching && list.length <= 0 || !models.isFetching && loadData.isLoad) {
			dispatch(requestList())
			dispatch(init({ isLoad: false }))
		}
	})

	const Loading = () => <div>Пожалуйтса подождите, загрузка</div>

	const selectHandler = (event, item) => {
		let selectedItems = [ ...selectedList.list ]
		if (event.target.checked) {
			selectedItems = [ ...selectedItems, item ]
			dispatch(select({ list: selectedItems }))
		} else {
			const newSelectedItems = []
			selectedItems.filter(searchItem => {
				if (searchItem !== item) {
					newSelectedItems.push(searchItem)
				}
			})
			dispatch(select({ list: newSelectedItems }))
		}
	}

	const viewHandler = event => {
		event.preventDefault()
		if (selectedList.list && selectedList.list.length !== 0) {
			dispatch(push('/view/all'))
		}
	}

	return (
		<Page>
			<div className="b-list">
				<p>Вы можете выбрать несколько моделей используя чекбоксы и кнопку «Загрузить выбранное» или перейти по ссылки модели чтобы загрузить одну выбранную модель</p>
				<form action="">
					{list.length === 0 && <Loading />}
					{list.length !== 0 && <ul>
						{list.map(model => {
							return <li key={`model-item-${model.id}`}>
								<input type="checkbox" onChange={event => selectHandler(event, model.url)} />
								<Link className={'b-list__item-link'} to={`/view/${model.name}`} data={model.url}>
									{model.name}
								</Link>
							</li>
						})}
					</ul>
					}
					<button
						type={'button'}
						disabled={Boolean(selectedList.list && selectedList.list.length === 0)}
						onClick={event => viewHandler(event)}>
						Загрузить выбранные
					</button>
				</form>
			</div>
		</Page>
	)
}

ListPage.propTypes = {
	dispatch: PropTypes.func,
	models: PropTypes.object,
	loadData: PropTypes.object,
	selectedList: PropTypes.object,
}

function mapStateToProps (state) {
	return {
		models: state.list,
		loadData: state.init,
		selectedList: state.select
	}
}

function mapDispatchToProps (dispatch) {
	return {
		dispatch
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ListPage)
