import React from 'react'
import SceneContainer from '../Container/SceneContainer'

import Page from 'Molecule/Page'

const MainPage = () => (
	<Page>
		<SceneContainer name={'default'} />
	</Page>
)

export default MainPage
