import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { request } from '../../store/actions/request'
import { init } from '../../store/actions/init'

import Page from 'Molecule/Page'

const UploadPage = ({ dispatch, result }) => {
	const [ data, setValue ] = useState({ name: 'model', value: null })

	const changeHandler = (event) => {
		event.preventDefault()
		const file = event.target.files[0]
		setValue({ ...data, value: file })
	}

	const submitHandler = async (event) => {
		event.preventDefault()
		await dispatch(request('/upload', data))
		await dispatch(init({ isLoad: true }))
	}

	const errorStyles = {
		border: '1px solid red',
		color: 'red',
	}
	const successStyles = {
		border: '1px solid green',
		color: 'green',
	}

	const status = result.status ? result.status : null
	let messageStyles = { display: 'inline-block', margin: '8px 0', padding: '8px', borderRadius: '5px' }
	status === 'success' ? messageStyles = { ...messageStyles, ...successStyles } : messageStyles = { ...messageStyles, ...errorStyles }

	return (
		<Page>
			<div className="b-upload">
				<p>Здесь можно загрузить модели в офрмате <b>.OBJ</b></p>
				<form action="">
					<label htmlFor="file">Загрузить модель</label><br/>
					<input id="file" type="file" name={'model'} onChange={changeHandler} accept={'.obj'} />
					<button disabled={!data.value} onClick={(event) => submitHandler(event)}>Загрузить</button>
				</form>
				{result.details && result.details.message &&
				<div className="b-upload__error" style={messageStyles}>
					{result.details.message}
				</div>
				}
			</div>
		</Page>
	)
}

UploadPage.propTypes = {
	dispatch: PropTypes.func,
	result: PropTypes.object
}

function mapStateToProps (state) {
	return {
		result: state.request
	}
}

function mapDispatchToProps (dispatch) {
	return {
		dispatch
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadPage)
