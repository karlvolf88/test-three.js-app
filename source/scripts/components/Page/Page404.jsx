import React from 'react'

import Page from 'Molecule/Page'

const Page404 = () => (
	<Page>
		страница не найдена
	</Page>
)

export default Page404
