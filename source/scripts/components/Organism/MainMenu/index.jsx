import { Link } from 'react-router-dom'
import React from 'react'

const MainMenu = () => (
	<ul>
		<li><Link to={'/upload'}>Загрузить</Link></li>
	</ul>
)

export default MainMenu
