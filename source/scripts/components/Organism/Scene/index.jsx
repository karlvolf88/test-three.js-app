/*eslint-disable*/
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Scene, PerspectiveCamera, WebGLRenderer, BoxGeometry, MeshPhongMaterial, Mesh, DirectionalLight } from 'three'

import './index.scss'

const MainScene = ({ name }) => {
	const [ wrapper ] = useState({ name })

	useEffect(() => {
		const wrapperComponent = document.getElementById(wrapper.name)
		const wrapperComponentSize = wrapperComponent.getBoundingClientRect()

		const renderer = new WebGLRenderer() // способ рендера
		renderer.setSize(wrapperComponentSize.width, wrapperComponentSize.width / 1.5)

		const fov = 75 // поле зрения
		const aspect = 2 // соотношение сторон
		const near = 0.1 // пространство перед камерой
		const far = 5 // пространство перед камерой
		const camera = new PerspectiveCamera(fov, aspect, near, far)
		camera.position.z = 4

		const scene = new Scene()

		{
			const color = 0xFFFFFF;
			const intensity = 1;
			const light = new DirectionalLight(color, intensity);
			light.position.set(-1, 2, 4);
			scene.add(light);
		}

		const makeCube = (geometry, color, x, y = 1, z = 0)  => {
			const material = new MeshPhongMaterial({ color });
			const cube = new Mesh(geometry, material)
			scene.add(cube)
			cube.position.x = x
			cube.position.y = y
			cube.position.z = z
			return cube
		}

		const cubes = [
			makeCube(new BoxGeometry(1, 1, 1), 0x44aa88, 0),
			makeCube(new BoxGeometry(1, 1, 1), 0x8844aa, -2),
			makeCube(new BoxGeometry(1, 1, 1), 0xaa8844,  2),
		]

		renderer.render(scene, camera)
		wrapperComponent.appendChild( renderer.domElement )

		function resizeRendererToDisplaySize(renderer) {
			const canvas = renderer.domElement
			const pixelRatio = window.devicePixelRatio
			const width = canvas.clientWidth * pixelRatio | 0
			const height = canvas.clientHeight * pixelRatio | 0
			const needResize = canvas.width !== width || canvas.height !== height
			if (needResize) {
				renderer.setSize(width, height, false)
			}
			return needResize
		}

		const animate = (time) => {
			time *= 0.001 // время в секунды

			if (resizeRendererToDisplaySize(renderer)) {
				const canvas = renderer.domElement
				camera.aspect = canvas.clientWidth / canvas.clientHeight
				camera.updateProjectionMatrix()
			}

			cubes.forEach((cube) => {
				const speed = 1 + .1
				const rot = time * speed
				cube.rotation.x = rot
				cube.rotation.y = rot
			})

			renderer.render(scene, camera)

			requestAnimationFrame(animate) //запрос к барузеру для анимации
		}
		animate()
	})

	return (
		<div id={wrapper.name} className={'b-scene'} data-scene-name={name} />
	)
}

MainScene.propTypes = {
	name: PropTypes.string.isRequired
}

export default MainScene
