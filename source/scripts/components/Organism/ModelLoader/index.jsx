import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

import { init as modelInitializer } from 'Utils/threeUtils'

const ModelLoader = ({ id, url }) => {
	useEffect(() => {
		const wrapperComponent = document.getElementById(id)
		let objectList = null
		if (typeof url !== 'string') {
			//eslint-disable-next-line
			debugger
			objectList = [ ...url ]
		} else {
			objectList = [ url ]
		}
		modelInitializer(wrapperComponent, objectList)
	})

	return (
		<div className="b-loader" id={id} />
	)
}

ModelLoader.defaultProps = {
	id: 'canvas-container',
}

ModelLoader.propTypes = {
	url: PropTypes.array,
	id: PropTypes.string,
}

export default ModelLoader
