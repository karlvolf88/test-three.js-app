import React from 'react'
import Link from 'Container/LinkContainer'

import './index.scss'

const HeaderComponent = () => (
	<div className={'b-header'}>
		<div className="container">
			<div className="b-header__row">
				<div className="b-header__col">
					<Link to={'/'}><span>Лого</span></Link>
				</div>
				<div className="b-header__col">
					<ul>
						<li><Link to={'/upload'}>Загрузить</Link></li>
						<li><Link to={'/list'}>Список</Link></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
)

export default HeaderComponent
