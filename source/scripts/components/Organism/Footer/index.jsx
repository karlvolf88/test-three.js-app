import React from 'react'
import { Link } from 'react-router-dom'

import './index.scss'

const FooterComponent = () => (
	<div className={'b-footer'}>
		<div className="container">
			<div className="b-footer__row">
				<div className="b-footer__col">
				</div>
				<div className="b-footer__col">
					<Link to={'/'}><span>Лого</span></Link>
				</div>
			</div>
		</div>
	</div>
)

export default FooterComponent
