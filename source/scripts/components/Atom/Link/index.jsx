import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const LinkComponent = ({ text, to, children, trigger, onClick, data = {}, className }, props) => {
	const customProps = {
		className
	}
	const clickHandler = (event) => {
		if (onClick) onClick(event)
		trigger({ currentPage: to, data })
	}
	return (
		<Link
			{...props}
			{...customProps}
			to={to}
			onClick={() => clickHandler()}
		>{text || children}</Link>
	)
}

LinkComponent.propTypes = {
	children: PropTypes.any,
	trigger: PropTypes.func,
	onClick: PropTypes.func,
	text: PropTypes.string,
	className: PropTypes.string,
	to: PropTypes.string,
	data: PropTypes.any,
	'data-link': PropTypes.string,
}

export default LinkComponent
