import React from 'react'
import { connect } from 'react-redux'

import { page } from 'Store/actions/page'

import Link from 'Atom/Link'

const LinkContainer = (props) => (
	<Link {...props} />
)

function mapStateToProps (state) {
	return {
		...state
	}
}

function mapDispatchToProps (dispatch) {
	return {
		trigger: (status) => dispatch(page(status))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LinkContainer)
