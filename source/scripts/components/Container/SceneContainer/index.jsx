import React from 'react'
import { connect } from 'react-redux'

import { init as initialize } from '../../../store/actions/init'
import Scene from 'Organism/Scene'

const SceneContainer = (props) => (
	<Scene {...props} />
)

function mapStateToProps (state) {
	const { init } = state
	return {
		init
	}
}

function mapDispatchToProps (dispatch) {
	return {
		init: status => dispatch(initialize(status))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SceneContainer)
