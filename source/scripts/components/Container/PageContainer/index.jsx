import React from 'react'
import { connect } from 'react-redux'

import { init as initialize } from '../../../store/actions/init'
import Page from 'Molecules/Page'

const PageContainer = (props) => (
	<Page {...props} />
)

function mapStateToProps (state) {
	return {
		state
	}
}

function mapDispatchToProps (dispatch) {
	return {
		dispatch
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PageContainer)
