import React from 'react'
import { connect } from 'react-redux'

import Header from 'Organism/Header'

const HeaderContainer = (props) => (
	<Header {...props} />
)

function mapStateToProps (state) {
	const { page } = state
	return {
		page
	}
}

export default connect(mapStateToProps)(HeaderContainer)
