module.exports = {
	assets: false,
	builtAt: false,
	cached: false,
	cachedAssets: false,
	children: false,
	chunks: false,
	colors: true,
	depth: false,
	entrypoints: false,
	env: true,
	errors: true,
	errorDetails: true,
	hash: false,
	modules: false,
	moduleTrace: false,
	performance: true,
	publicPath: true,
	timings: true,
	version: true,
	warnings: false,
	optimizationBailout: true
}
