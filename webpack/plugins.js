const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HotModuleReplacementPlugin = require('webpack').HotModuleReplacementPlugin
const NoEmitOnErrorsPlugin = require('webpack').NoEmitOnErrorsPlugin

module.exports = [
	new HotModuleReplacementPlugin(),
	new NoEmitOnErrorsPlugin(),
	new MiniCssExtractPlugin({
		filename: '[name].css',
		chunkFilename: '[id].css',
	})
]
