const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const AutoPreFixer = require('autoprefixer')
const FlexBugFixer = require('postcss-flexbugs-fixes')

const styles = {
	test: /\.(scss|css)$/,
	include: path.resolve('./source'),
	use: [
		'css-hot-loader',
		MiniCssExtractPlugin['loader'],
		'css-loader',
		{
			loader: 'postcss-loader',
			options: {
				sourceMap: true,
				plugins: [
					AutoPreFixer({
						grid: 'autoplace',
						flex: true
					}),
					FlexBugFixer()
				]
			}
		},
		{
			loader: 'sass-loader',
		},
	]
}

const lint = {
	test: /\.js?x$/,
	enforce: 'pre',
	exclude: /node_modules/,
	loader: 'eslint-loader',
	options: {
		cache: true,
		failOnError: true,
	}
}

const scripts = {
	test: /\.(js|jsx)$/,
	exclude: /node_modules/,
	include: path.resolve('./source/scripts'),
	use: [
		{
			loader: 'babel-loader',
			options: {
				cacheDirectory: true,
				presets: [
					'@babel/preset-env',
					'@babel/preset-react'
				],
				'plugins': [
					'@babel/plugin-transform-runtime',
					'@babel/plugin-proposal-class-properties'
				]
			}
		}
	]
}

const svgImages = {
	test: /\.svg$/,
	loader: 'svg-inline-loader'
}

module.exports = [
	lint,
	scripts,
	styles,
	svgImages
]
