const { resolve } = require('path')

const SOURCE_PATH = './source'

module.exports = {
	modules: [ 'node_modules' ],
	extensions: [ '.js', '.jsx', '.scss' ],
	alias: {
		Root: resolve('./'),
		Components: resolve(`${SOURCE_PATH}/scripts/components`),
		Store: resolve(`${SOURCE_PATH}/scripts/store`),
		Container: resolve(`${SOURCE_PATH}/scripts/components/Container`),
		Atom: resolve(`${SOURCE_PATH}/scripts/components/Atom`),
		Molecule: resolve(`${SOURCE_PATH}/scripts/components/Molecule`),
		Organism: resolve(`${SOURCE_PATH}/scripts/components/Organism`),
		Pages: resolve(`${SOURCE_PATH}/scripts/components/Page`),
		Utils: resolve(`${SOURCE_PATH}/scripts/utils`),
		Styles: resolve(`${SOURCE_PATH}/styles/`),
		Images: resolve(`${SOURCE_PATH}/images`)
	}
}
