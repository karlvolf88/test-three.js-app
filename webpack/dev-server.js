const { join, resolve } = require('path')

module.exports = {
	contentBase: resolve('./public'),
	compress: true,
	historyApiFallback: true,
	hot: true,
	hotOnly: true,
	index: join(__dirname, 'public/index.html'),
	open: true,
	port: '3000',
	stats: {
		all: false,
		errors: true,
		errorDetails: true,
		modules: false,
		moduleTrace: false,
		timings: true,
		warnings: true
	},
	watchContentBase: true
}

